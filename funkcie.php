<?php

function hlavicka($nadpis) {
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $nadpis; ?></title>
		<link href="styly.css" rel="stylesheet">
	</head>

<body>

<div id="container">
<header>
<h1><?php echo $nadpis; ?></h1>
</header>

	<nav>
	 <a href="index.php">Domov</a>
	 <a href="galeria.php">Galéria</a>
	 <a href="login.php">Prihlásenie</a>
	</nav>
	<div id="content">	
<?php
}

function footer() {
	?>
	</div> <!--koniec content-u -->

	<footer>
		<div id="footer-nav">
		 <a href="info.php">O projekte</a>	
		 <a href="terms.php">Podmienky používania</a>
		 <a href="kontakt.php">Kontakt</a>
		</div>

		&copy;Peter Gubik, 2013
	</footer>
  </div>
<?php	
}

function spoj_s_db() {
	if ($link = mysql_connect('localhost', 'root', '')) {
		if (mysql_select_db('projekt', $link)) {
			mysql_query("SET CHARACTER SET 'utf8'", $link); 
			return $link;
		} else {
			
			return false;
		}
	} else {
		
		return false;
	}
}

function over_pouzivatela($name, $pass) {
	if ($link = spoj_s_db()) {
		$sql = "SELECT * FROM users WHERE nick='$name' AND heslo=md5('$pass')";
		$result = mysql_query($sql, $link);
		if ($result && (mysql_num_rows($result) > 0) ) {
			$row = mysql_fetch_assoc($result);
			mysql_free_result($result);
			return $row;
		} else {
			// dopyt sa NEpodarilo vykonať, resp. používateľ neexistuje!
			return false;
		}
	} else {
		// NEpodarilo sa spojiť s databázovým serverom!
		return false;
	}
}

function registruj() {
    if ($link = spoj_s_db()) {
        $sql = "INSERT INTO users SET nick='" . addslashes(strip_tags($_POST['nick-reg'])) . "',
		heslo='" . md5($_POST['heslo-first']) . "',
    	admin = 0 ";
        $result = mysql_query($sql, $link); // vykonaj dopyt
        if ($result) {
            // dopyt sa podarilo vykonať
        echo '<p>Registrácia bola úspešná. Môžete sa prihlásiť.</p>'. "\n"; 
         } else {
            // NEpodarilo sa vykonať dopyt!
            echo '<p class="chyba">Nastala chyba pri registrácii.</p>';
      }
        mysql_close($link);
    } else {
        // NEpodarilo sa spojiť s databázovým serverom alebo vybrať databázu!
        echo '<p class="chyba">Nepodarilo sa spojiť s databázovým serverom!</p>';
    }
}

function dobry_retazec($co, $min, $max) {
    return (strlen(trim($co)) >= $min) && (strlen(trim($co)) <= $max);          
}

?>