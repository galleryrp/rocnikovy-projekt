<?php include('funkcie.php');
hlavicka('Podmienky používania');
?>


	<ol id="terms">
		<li>Na stránku je zakázané pridávať erotický, násilný, diskriminačný, rasistický, nenávistný a inak nevhodný obsah. Takýto obsah - fotografie aj komentáre - bude odstránený administrátorom.</li>
		<li>Užívateľ sa zaväzuje chrániť si svoje prihlasovacie údaje.</li>
		<li>Je zakázané obťažovať, ponižovať, zastrašovať, vyhrážať sa alebo prenasledovať používateľov.</li>
		<li>Užívateľ je zodpovedný za všetok textový a grafický obsah, všetky informácie, prihlasovacie údaje a ostatné dáta, ktoré zverejní na stránke.</li>
		<li>Užívateľ si je vedomý, že administrátor stránky vidí všetok obsah, ktorý užívateľ pridal na stránku, a v prípade, že tento obsah považuje za nevhodný, môže ho vymazať.</li>
		<li>Užívateľ nesmie posielať ostatným užívateľom nevyžiadané reklamné alebo obťažujúce komentáre alebo iný obsah.</li>
		<li>Užívateľ nesmie vytvárať účty za účelom používania automatov, skriptov, botov, spiderov alebo crawlerov.</li>
		<li>Užívateľ nesmie vyžadovať, zbierať alebo iným spôsobom získavať prihlasovacie údaje ostatných užívateľov a nesmie tieto údaje používať.</li>
	</ol>


<?php
footer();
?>