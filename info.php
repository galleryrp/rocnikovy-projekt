<?php include('funkcie.php');
hlavicka('Informácie o projekte');
?>



	<div id="info">
		<p>Na tejto stránke sa nachádzajú informácie o projekte. Projekt je vo vývoji, takže informácie budú postupne pribúdať.</p>

		Dokumentácia na stiahnutie: <a href="spec-SK.pdf">Slovenčina</a> / <a href="spec-EN.pdf">English</a>
		<h3>Ako sa orientovať na stránke</h3>
		<p>
			Navigačné menu v hlavičke:<br>
			<strong>Domov</strong> - Domovská stránka<br>
			<strong>Galéria</strong> - V tejto časti budú zobrazené albumy a fotografie podľa toho, kto je prihlásený (podrobnosti v špecifikácii).<br>
			<strong>Prihlásenie</strong> - Táto časť slúži na prihlásenie užívateľa. V prípade, že ešte nemá vytvorený účet, môže sa registrovať.
		</p>

		<p>
			Navigačné menu v pätičke:<br>
			<strong>O projekte</strong> - Stránka, na ktorej sa dozviete podrobnosti o projekte.<br>
			<strong>Podmienky používania</strong> - Časť, ktorá užívateľov oboznamuje s ich právami a povinnosťami počas používania stránky.<br>
			<strong>Kontakt</strong> - Kontaktné informácie na autora projektu.
		</p>
	</div>

<?php
footer();
?>