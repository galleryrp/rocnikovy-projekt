<?php 
session_start();
include('funkcie.php');
hlavicka('Prihlásenie');
?>



<?php
	//prihlasovaci formular
	if (isset($_POST["nick"]) && isset($_POST["heslo"]) && $pouzivatel = over_pouzivatela($_POST["nick"], $_POST["heslo"])) {
		$_SESSION['nick'] = $pouzivatel['nick'];
		$_SESSION['admin'] = $pouzivatel['admin'];
		}

	elseif (isset($_POST['odhlas'])) { 
		session_unset();
		session_destroy();
	}


	if (isset($_SESSION['nick'])) {
	?>
	<p>Vitajte v systéme.</p>
	<?php

		if ($_SESSION['admin']) {
			echo '<p>Ste prihlásený ako administrátor.</p>';
			}

		else echo '<p>Ste prihlásený ako obyčajný užívateľ.</p>';

?>
	<form method="post"> 
	  <p> 
	    <input name="odhlas" type="submit" id="odhlas" value="Odhlásenie"> 
	  </p> 
	</form> 

<?php
} else { 
?>
	<form method="post">
		<fieldset>
			<legend>Prihlásenie</legend>
		<p>
		<label for="nick">Prihlasovacie meno:</label> 
		<input name="nick" type="text" size="30" maxlength="30" id="nick" value="<?php if (isset($_POST["nick"])) echo $_POST["nick"]; ?>" ><br>
		<label for="heslo">Heslo:</label> 
		<input name="heslo" type="password" size="30" maxlength="30" id="heslo"> 
		</p>

		<p>
			<input name="submit" type="submit" id="submit" value="Prihlás ma">
		</p>
	</fieldset>
	</form>	
<?php 
}
	if ( isset($_POST["nick"]) && isset($_POST["heslo"]) && over_pouzivatela($_POST["nick"], $_POST["heslo"])== false)   {
		echo '<p class="chyba">Nesprávne zadané meno alebo heslo!</p>';
	}
?>

<?php 
	//registracny formular
if (empty($_SESSION['nick'])) {
	?>
	<form method="post">
		<fieldset>
			<legend>Registrácia</legend>
		<p>
		<label for="nick-reg">Prihlasovacie meno:</label> 
		<input name="nick-reg" type="text" size="30" maxlength="30" id="nick-reg" value="" ><br>
		<label for="heslo-first">Heslo:</label> 
		<input name="heslo-first" type="password" size="30" maxlength="30" id="heslo-first"><br>
		<label for="heslo-sec">Opakuj heslo:</label> 
		<input name="heslo-sec" type="password" size="30" maxlength="30" id="heslo-sec">		 
		</p>

		<p>
			<input name="registruj" type="submit" id="registruj" value="Registrácia">
		</p>
	</fieldset>
	</form>	

<?php
	if (isset($_POST['registruj']) && 
	    isset($_POST['nick-reg']) && dobry_retazec($_POST['nick-reg'], 3, 30) && 
	    isset($_POST['heslo-first']) && dobry_retazec($_POST['heslo-first'], 3, 30) && 
	    isset($_POST['heslo-sec']) && dobry_retazec($_POST['heslo-sec'], 3, 30) && 
	    ($_POST['heslo-first'] == $_POST['heslo-sec']) ) { 
	// pridanie používateľa
		registruj();  
	} else { 
		if (isset ($_POST['registruj'])) {
			echo '<span class="chyba">Nezadali ste všetky údaje, resp. nemajú správny formát!</span>';
			if ($_POST['heslo-first'] != $_POST['heslo-sec'])
				echo '<p class="chyba">Heslá musia byť rovnaké!</p>'; 
		}
	}

}
?>

<?php
footer();
?>