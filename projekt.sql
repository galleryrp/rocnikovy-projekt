-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Po 09.Dec 2013, 17:54
-- Verzia serveru: 5.5.24-log
-- Verzia PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `projekt`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `nick` varchar(30) CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `heslo` varchar(50) CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `admin` tinyint(2) NOT NULL,
  `user_id` smallint(7) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`nick`, `heslo`, `admin`, `user_id`) VALUES
('user', 'ee11cbb19052e40b07aac0ca060c23ee', 0, 1),
('admin', '21232f297a57a5a743894a0e4a801fc3', 1, 2),
('nick', 'e2e42a07550863f8b67f5eb252581f6d', 0, 3),
('newuser', '955db0b81ef1989b4a4dfeae8061a9a6', 0, 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
